import 'package:flutter/material.dart';
import 'package:smartgreen/screens/diaryScreen/diaryScreen.dart';
import 'package:smartgreen/screens/homeScreen/appHomeScreen.dart';
import 'package:smartgreen/screens/loginScreen/loginScreen.dart';
import 'package:smartgreen/theme/appTheme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CTT App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
        textTheme: AppTheme.textTheme,
        platform: TargetPlatform.iOS,
      ),
      // home: AppHomeScreen(),
      initialRoute: '/',
      routes: {
        '/': (BuildContext context) => LoginScreen(),
        '/home': (BuildContext context) => AppHomeScreen(),
        '/index': (BuildContext context) => DiaryScreen()
      },
    );
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}