class MealsListData {
  String imagePath;
  String titleTxt;
  String startColor;
  String endColor;
  List<String> meals;
  int kacl;

  MealsListData({
    this.imagePath = '',
    this.titleTxt = '',
    this.startColor = "",
    this.endColor = "",
    this.meals,
    this.kacl = 0,
  });

  static List<MealsListData> tabIconsList = [
    MealsListData(
      imagePath: 'assets/images/cabage.png',
      titleTxt: 'Xà lách',
      kacl: 525,
      // meals: ["Bread,", "Peanut butter,", "Apple"],
      meals: ["Số lượng: ", "10 cây"],
      startColor: "#FA7D82",
      endColor: "#FFB295",
    ),
    MealsListData(
      imagePath: 'assets/images/latuce.png',
      titleTxt: 'Cải xanh',
      kacl: 602,
      // meals: ["Salmon,", "Mixed veggies,", "Avocado"],
      meals: ["Số lượng: ", "15 cây"],
      startColor: "#738AE6",
      endColor: "#5C5EDD",
    ),
    MealsListData(
      imagePath: 'assets/images/caiNgot.png',
      titleTxt: 'Cải ngọt',
      kacl: 850,
      // meals: ["Recommend:", "800 kcal"],
      meals: ["Số lượng: ", "30 cây"],
      startColor: "#FE95B6",
      endColor: "#FF5287",
    ),
    MealsListData(
      imagePath: 'assets/images/dinner.png',
      titleTxt: 'Dinner',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#6F72CA",
      endColor: "#1E1466",
    ),
  ];
}
