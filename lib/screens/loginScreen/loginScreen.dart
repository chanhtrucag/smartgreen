import 'dart:ui';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:smartgreen/screens/diaryScreen/diaryScreen.dart';
import 'package:smartgreen/screens/diaryScreen/waterView.dart';
import 'package:smartgreen/screens/homeScreen/appHomeScreen.dart';
import 'package:flutter/services.dart';
import 'package:smartgreen/theme/appTheme.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var _txt = TextEditingController();
  String result = "HI THERE";
  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        _txt.text = qrResult;
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          _txt.text = "Camera permission was denied";
        });
      } else {
        setState(() {
          _txt.text = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        _txt.text = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        _txt.text = "Unknown Error $ex";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    ScreenUtil.instance =
        ScreenUtil(width: 750, height: 1334, allowFontScaling: true);
    return new Scaffold(
      backgroundColor: Colors.white,
      // resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Center(
            child: Image.asset(
              'assets/images/image_04.jpg',
              width: ScreenUtil.getInstance().width,
              height: ScreenUtil.getInstance().height,
              fit: BoxFit.fill,
            ),
          ),
          Center(
            child: ClipRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              ),
            ),
          ),
          Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10),
                ),
                Center(
                  child: Image.asset(
                    'assets/images/logo.png',
                    fit: BoxFit.fill,
                    width: ScreenUtil.getInstance().setWidth(300),
                    height: ScreenUtil.getInstance().setHeight(300),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25.0),
                    child: TextField(
                      controller: _txt,
                      style: TextStyle(
                        fontFamily: AppTheme.fontName,
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: AppTheme.darkText,
                      ),
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          // prefixIcon: Icon(Icons.devices),
                          // fillColor: Color(0x77DD77).withOpacity(0.5),
                          hintText: "Nhập mã vườn",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0))),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(5),
                ),
                Center(
                  child: Padding(
                    // padding: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.symmetric(horizontal: 25.0),
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/home');
                      },
                      child: Text("ĐĂNG NHẬP",
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: AppTheme.fontName,
                              fontWeight: FontWeight.bold)),
                      // color: Color(0xffff2d55),
                      color: Color(0xFF11d127),
                      // elevation: 0,
                      minWidth: 400,
                      height: 60,
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                    ),
                  ),
                ),
                Center(
                  // padding: EdgeInsets.only(top: 20),
                  // padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: MaterialButton(
                    // onPressed: () {},
                    onPressed: () {
                      _scanQR();
                    },
                    // children: <Widget>[
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.camera_alt,
                          color: Colors.white,
                        ),
                        Text("Quét mã QR",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: AppTheme.fontName,
                                color: Colors.white,
                                fontWeight: FontWeight.w400)),
                      ],
                    ),

                    // color: Color(0xffff2d55),
                    // elevation: 0,
                    minWidth: 400,
                    // height: 60,
                    // textColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                  ),
                ),
                // Center(
                //   child: InkWell(
                //     child: Container(
                //       width: ScreenUtil.getInstance().setWidth(330),
                //       height: ScreenUtil.getInstance().setHeight(100),
                //       decoration: BoxDecoration(
                //         // gradient: LinearGradient(
                //         //     colors: [Color(0xFF17ead9), Color(0xFF6078da)]),
                //         color: Color(0x77DD77).withOpacity(1),
                //         borderRadius: BorderRadius.circular(6.0),
                //         // boxShadow: [
                //         //   BoxShadow(
                //         //       color: Color(0x9CF196),
                //         //       offset: Offset(0.0, 8.0),
                //         //       blurRadius: 8.0),
                //       ),
                //       child: Material(
                //         color: Colors.transparent,
                //         child: InkWell(
                //           onTap: () {
                //             // Navigator.push(context, MaterialPageRoute(builder: (context) => AppHomeScreen()),);
                //             _scanQR();
                //           },
                //           child: Center(
                //             child: Text("ĐĂNG NHẬP",
                //                 style: TextStyle(
                //                     color: Colors.white,
                //                     fontWeight: FontWeight.bold,
                //                     fontSize:
                //                         ScreenUtil.getInstance().setSp(30))),
                //           ),
                //         ),
                //       ),
                //     ),
                //   ),
                // )
              ],
            ),
          )

          // Column(
          //   crossAxisAlignment: CrossAxisAlignment.end,
          //   children: <Widget>[
          //     Padding(
          //       padding: EdgeInsets.only(top: 40.0),
          //       child: Image.asset("assets/image_04.jpg", fit: BoxFit.fitHeight,),
          //     )
          //   ],
          // )
        ],
      ),
    );
  }
}
